#!/bin/bash

rm -rf pkg/ src/ xserver/
pacman -Q xorgproto-git &> /dev/null || yay -S xorgproto-git --noconfirm
MAKEFLAGS="-j$(nproc)" makepkg -s --noconfirm --skippgpcheck
rm -rf *debug*.pkg.tar.zst
zip -r xserver-pkg.zip *.pkg.tar.zst
